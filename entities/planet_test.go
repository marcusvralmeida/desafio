package entities

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPlanetValidation(t *testing.T) {
	validPlanet := Planet{
		Name:    "Hoth",
		Climate: "frozen",
		Terrain: "tundra, ice caves, mountain ranges",
	}

	invalidPlanet := Planet{
		Name:    "",
		Climate: "frozen",
		Terrain: "tundra, ice caves, mountain ranges",
	}

	t.Run("success", func(t *testing.T) {
		err := validPlanet.IsValid()
		assert.NoError(t, err)
	})
	t.Run("fail-invalid-planet", func(t *testing.T) {
		err := invalidPlanet.IsValid()
		assert.Error(t, err)
		assert.Equal(t, "'name' is required.", err.Error())
	})
}
