package entities

import (
	"errors"
	"fmt"
	"strings"

	"github.com/mongodb/mongo-go-driver/bson/primitive"
	validator "gopkg.in/go-playground/validator.v9"
)

// Planet representa a entidade Planeta no sistema.
type Planet struct {
	ID              primitive.ObjectID `json:"id" bson:"_id,omitempt"`
	Name            string             `json:"name" bson:"name" validate:"required"`
	Climate         string             `json:"climate" bson:"climate"`
	Terrain         string             `json:"terrain" bson:"terrain"`
	FilmAppearances int                `json:"film_appearances"`
}

// NewPlanet construtor para Planet.
func NewPlanet(name, climate, terrain string, appearances int) *Planet {
	return &Planet{
		Name:            name,
		Climate:         climate,
		Terrain:         terrain,
		FilmAppearances: appearances,
	}
}

// IsValid verifica se a entidade Planet é válida.
func (p Planet) IsValid() error {
	validate := validator.New()
	err := validate.Struct(&p)
	if err != nil {
		errs := make([]string, 0)
		if castedObject, ok := err.(validator.ValidationErrors); ok {
			for _, err := range castedObject {
				switch err.Tag() {
				case "required":
					errs = append(errs, fmt.Sprintf("'%s' is required.", strings.ToLower(err.Field())))
				}
			}
		}
		return errors.New(strings.Join(errs, ", "))
	}
	return nil
}

// PlanetPaginator struct cuja função é transportar as informações para realizar a paginação através das camadas da aplicação.
type PlanetPaginator struct {
	Total    int
	Next     int
	Previous int
	PerPage  int
	Results  []*Planet
}

// PreviousPage função para montar o link para página anterior na paginação.
func (p PlanetPaginator) PreviousPage(host string, path string) string {
	if p.Previous > 0 {
		if p.PerPage > 0 {
			return fmt.Sprintf("%s%s?page=%d&per_page=%d", host, path, p.Previous, p.PerPage)
		} else {
			return fmt.Sprintf("%s%s?page=%d", host, path, p.Previous)
		}
	}
	return ""
}

// NextPage função para montar o link para a próxima pagina na paginação.
func (p PlanetPaginator) NextPage(host string, path string) string {
	if p.Next > 0 {
		if p.PerPage > 0 {
			return fmt.Sprintf("%s%s?page=%d&per_page=%d", host, path, p.Next, p.PerPage)
		} else {
			return fmt.Sprintf("%s%s?page=%d", host, path, p.Next)
		}

	}
	return ""
}
