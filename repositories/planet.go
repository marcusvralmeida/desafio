package repositories

import (
	"context"
	"log"
	"strings"

	"bitbucket.org/marcusvralmeida/desafio-b2w/entities"
	errors "bitbucket.org/marcusvralmeida/desafio-b2w/errors"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/options"
)

const (
	// COLLECTION constante com nome da Collection utilizada no MongoDB para persistência dos dados.
	COLLECTION = "planets"
)

// PlanetRepository interface com a definição dos métodos do PlanetRepository.
type PlanetRepository interface {
	FindAll(ctx context.Context, page, pageSize int) (*entities.PlanetPaginator, error)
	FindByID(ctx context.Context, id string) (*entities.Planet, error)
	FindByName(ctx context.Context, name string) (*entities.Planet, error)
	Save(ctx context.Context, planet *entities.Planet) (*entities.Planet, error)
	Delete(ctx context.Context, id string) error
}

// MongoPlanetRepository definição da struct para a implementação do PlanetRepository utilizando MongoDB.
type MongoPlanetRepository struct {
	db *mongo.Database
}

// NewPlanetRepository construtor.
func NewPlanetRepository(database *mongo.Database) PlanetRepository {
	return &MongoPlanetRepository{
		db: database,
	}
}

// FindAll função para retornar todos os Planets.
// Recebe como parâmetros ctx context.Context e os parâmentos page e pageSize int, utilizados para paginação.
// O retorno é um ponteiro para a struct entities.Paginator (Value Object) com as informações para paginação.
// A paginação implementada nesse método foi realizada utilizando as funções Skip e Limit da API do MongoDB,
// apesar de não ser muito eficiente, por recuperar todos os registros e depois realizar o Skip. Como temos um
// número de registros (Planets) não muito grande podemos utilizar essa implementação.
func (r *MongoPlanetRepository) FindAll(ctx context.Context, page, pageSize int) (*entities.PlanetPaginator, error) {
	var results []*entities.Planet
	results = make([]*entities.Planet, 0)
	total, err := r.db.Collection(COLLECTION).CountDocuments(ctx, nil, nil)
	if err != nil {
		log.Println("MongoPlanetRepository - FindAll: ", err.Error())
		return nil, err
	}

	findOptions := options.Find()
	findOptions.SetLimit(int64(pageSize))
	findOptions.SetSkip(int64(pageSize * (page - 1)))
	cur, err := r.db.Collection(COLLECTION).Find(ctx, nil, findOptions)
	if err != nil {
		log.Println("MongoPlanetRepository - FindAll: ", err.Error())
		return nil, err
	}

	for cur.Next(ctx) {
		var elem entities.Planet
		err := cur.Decode(&elem)
		if err != nil {
			log.Println("MongoPlanetRepository - FindAll: ", err.Error())
			return nil, err
		}
		results = append(results, &elem)
	}
	if err := cur.Err(); err != nil {
		log.Println("MongoPlanetRepository - FindAll: ", err.Error())
		return nil, err
	}
	if err := cur.Close(ctx); err != nil {
		log.Println("MongoPlanetRepository - FindAll: ", err.Error())
		return nil, err
	}

	var next, previous int
	if pageSize == len(results) {
		next = page + 1
	}
	if page > 1 {
		previous = page - 1
	}
	return &entities.PlanetPaginator{
		Total:    int(total),
		Next:     next,
		Previous: previous,
		Results:  results,
	}, nil
}

// FindByID função para recuperar um Planet por Id
// Recebe como parâmetros ctx context.Context e o id do Planet como string.
// Retorna um ponteiro para a entidade Planet quando for encontrada e um erro quando não for encontrado.
func (r *MongoPlanetRepository) FindByID(ctx context.Context, id string) (*entities.Planet, error) {
	var result *entities.Planet

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println("MongoPlanetRepository - FindByID: ", err.Error())
		return nil, err
	}

	filter := bson.D{{"_id", objectID}}
	err = r.db.Collection(COLLECTION).FindOne(ctx, filter).Decode(&result)
	if err != nil {
		log.Println("MongoPlanetRepository - FindByID: ", err.Error())
		return nil, errors.ErrNotFound
	}
	return result, nil
}

// FindByName função para recuperar um Planet por Name (case insensitive).
// Recebe como parâmetros ctx context.Context e name do Planet como string.
// Retorna um ponteiro para a entidade Planet quando for encontrada e um erro quando não for encontrado.
func (r *MongoPlanetRepository) FindByName(ctx context.Context, name string) (*entities.Planet, error) {
	var result *entities.Planet
	// filter case insensitive
	filter := bson.D{{"name", primitive.Regex{Pattern: name, Options: "i"}}}
	err := r.db.Collection(COLLECTION).FindOne(ctx, filter).Decode(&result)
	if err != nil {
		log.Println("MongoPlanetRepository - FindByName: ", err.Error())
		return nil, errors.ErrNotFound
	}
	return result, nil
}

// Save função para persistir uma entidade Planet no banco de dados.
// Recebe como parâmetros ctx context.Context e o ponteiro para a entidade Planet (entities.Planet).
// Retorna a entidade Planet salva, quando houver sucesso. Caso contrário retorna um erro.
func (r *MongoPlanetRepository) Save(ctx context.Context, planet *entities.Planet) (*entities.Planet, error) {
	planet.ID = primitive.NewObjectID()
	planet.Name = strings.Title(strings.ToLower(planet.Name))
	_, err := r.db.Collection(COLLECTION).InsertOne(ctx, planet)
	if err != nil {
		log.Println("MongoPlanetRepository - Save: ", err.Error())
		return nil, err
	}
	planetCreated, err := r.FindByID(ctx, planet.ID.Hex())
	if err != nil {
		log.Println("MongoPlanetRepository - Save: ", err.Error())
		return nil, err
	}
	return planetCreated, nil
}

// Delete função para apagar uma entidade Planet no banco de dados.
// Recebe como parâmetros ctx context.Context e o id da entidade Planet como string.
// Retorna um erro caso a operação não seja realizada com sucesso. Caso contrário retorna nil.
func (r *MongoPlanetRepository) Delete(ctx context.Context, id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println("MongoPlanetRepository - Delete: ", err.Error())
		return err
	}

	filter := bson.D{{"_id", objectID}}
	deleteResult, err := r.db.Collection(COLLECTION).DeleteOne(ctx, filter)
	if err != nil {
		log.Println("MongoPlanetRepository - Delete: ", err.Error())
		return err
	}
	if deleteResult.DeletedCount == 0 {
		return errors.ErrNotFound
	}
	return nil
}
