package repositories

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"

	"bitbucket.org/marcusvralmeida/desafio-b2w/entities"

	"github.com/stretchr/testify/assert"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/readpref"

	"github.com/ory/dockertest"
	"github.com/ory/dockertest/docker"
)

const (
	TESTING_DATABASE = "sw_testing"
)

var client *mongo.Client
var planetsID []string

func setupDatabase() {
	planetsID = make([]string, 0)
	collection := client.Database(TESTING_DATABASE).Collection(COLLECTION)
	alderaan := entities.Planet{
		ID:              primitive.NewObjectID(),
		Name:            "Alderaan",
		Climate:         "temperate",
		Terrain:         "grasslands, mountains",
		FilmAppearances: 2,
	}
	hoth := entities.Planet{
		ID:              primitive.NewObjectID(),
		Name:            "Hoth",
		Climate:         "frozen",
		Terrain:         "tundra, ice caves, mountain ranges",
		FilmAppearances: 1,
	}
	naboo := entities.Planet{
		ID:              primitive.NewObjectID(),
		Name:            "Naboo",
		Climate:         "terrain",
		Terrain:         "grassy hills, swamps, forests, mountains",
		FilmAppearances: 4,
	}
	planetsID = append(planetsID, alderaan.ID.Hex(), hoth.ID.Hex(), naboo.ID.Hex())

	planets := []interface{}{alderaan, hoth, naboo}
	_, err := collection.InsertMany(context.TODO(), planets)
	if err != nil {
		log.Fatal("Could not load fixture data")
	}
}

// TestMain ...
func TestMain(m *testing.M) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}
	// pulls an image, creates a container based on it and runs it
	options := &dockertest.RunOptions{
		Repository:   "mongo",
		Tag:          "4-xenial",
		Cmd:          []string{"mongod", "--smallfiles", "--port", "37017"},
		ExposedPorts: []string{"37017"},
		PortBindings: map[docker.Port][]docker.PortBinding{
			"37017/tcp": {{HostIP: "", HostPort: "37017"}},
		},
	}
	database, err := pool.RunWithOptions(options)
	database.Expire(60)
	if err != nil {
		log.Fatalf("Could not start database: %s", err)
	}

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	if err := pool.Retry(func() error {
		ctx := context.TODO()
		var err error
		connString := fmt.Sprintf("mongodb://localhost:%s", database.GetPort("37017/tcp"))
		client, err = mongo.Connect(ctx, connString)
		if err != nil {
			return err
		}
		return client.Ping(ctx, readpref.Primary())
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}
	setupDatabase()
	code := m.Run()

	// You can't defer this because os.Exit doesn't care for defer
	if err := pool.Purge(database); err != nil {
		log.Fatalf("Could not purge database: %s", err)
	}

	os.Exit(code)
}

//TestFindAll ...
func TestFindAll(t *testing.T) {
	repository := NewPlanetRepository(client.Database(TESTING_DATABASE))
	t.Run("success-page-1", func(t *testing.T) {
		page := 1
		itemsPerPage := 2
		results, err := repository.FindAll(context.TODO(), page, itemsPerPage)

		expectedLength := 2
		nextPage := 2
		previousPage := 0
		total := len(planetsID)

		assert.NoError(t, err)
		assert.Equal(t, total, results.Total)
		assert.Equal(t, expectedLength, len(results.Results))
		assert.Equal(t, previousPage, results.Previous)
		assert.Equal(t, nextPage, results.Next)
	})
	t.Run("success-page-2", func(t *testing.T) {
		page := 2
		itemsPerPage := 2
		results, err := repository.FindAll(context.TODO(), page, itemsPerPage)

		expectedLength := 1
		nextPage := 0
		previousPage := 1
		total := len(planetsID)

		assert.NoError(t, err)
		assert.Equal(t, total, results.Total)
		assert.Equal(t, expectedLength, len(results.Results))
		assert.Equal(t, previousPage, results.Previous)
		assert.Equal(t, nextPage, results.Next)
	})
}

func TestFindById(t *testing.T) {
	repository := NewPlanetRepository(client.Database(TESTING_DATABASE))
	t.Run("success", func(t *testing.T) {
		planet, err := repository.FindByID(context.TODO(), planetsID[0])
		assert.NoError(t, err)
		assert.Equal(t, planetsID[0], planet.ID.Hex())
		assert.Equal(t, "Alderaan", planet.Name)
	})
	t.Run("fail-to-convert-string-to-object-id", func(t *testing.T) {
		planet, err := repository.FindByID(context.TODO(), "109283")
		assert.Error(t, err)
		assert.Nil(t, planet)
	})
	t.Run("fail-planet-not-found", func(t *testing.T) {
		inexistentID := primitive.NewObjectID()
		planet, err := repository.FindByID(context.TODO(), inexistentID.Hex())
		assert.Error(t, err)
		assert.Equal(t, "The requested Planet is not found", err.Error())
		assert.Nil(t, planet)
	})
}

func TestFindByName(t *testing.T) {
	repository := NewPlanetRepository(client.Database(TESTING_DATABASE))
	t.Run("success", func(t *testing.T) {
		planetName := "Hoth"
		planet, err := repository.FindByName(context.TODO(), planetName)
		assert.NoError(t, err)
		assert.Equal(t, planetName, planet.Name)
	})
	t.Run("success-case-insensitive", func(t *testing.T) {
		planetName := "HOTH"
		planet, err := repository.FindByName(context.TODO(), planetName)
		assert.NoError(t, err)
		assert.Equal(t, "Hoth", planet.Name)
	})
	t.Run("fail-planet-not-found", func(t *testing.T) {
		inexistentPlanet := "Marte"
		planet, err := repository.FindByName(context.TODO(), inexistentPlanet)
		assert.Error(t, err)
		assert.Equal(t, "The requested Planet is not found", err.Error())
		assert.Nil(t, planet)
	})
}

func TestSave(t *testing.T) {
	repository := NewPlanetRepository(client.Database(TESTING_DATABASE))
	t.Run("success", func(t *testing.T) {
		earth := &entities.Planet{
			Name:            "Earth",
			Climate:         "temperate",
			Terrain:         "grasslands, mountains",
			FilmAppearances: 0,
		}
		planetCreated, err := repository.Save(context.TODO(), earth)
		assert.NoError(t, err)
		assert.NotNil(t, planetCreated.ID)
		assert.Equal(t, earth.Name, planetCreated.Name)
		assert.Equal(t, earth.Climate, planetCreated.Climate)
	})
}

func TestDelete(t *testing.T) {
	collection := client.Database(TESTING_DATABASE).Collection(COLLECTION)
	marte := entities.Planet{
		ID:              primitive.NewObjectID(),
		Name:            "Marte",
		Climate:         "hot",
		Terrain:         "desert",
		FilmAppearances: 1,
	}
	_, err := collection.InsertOne(context.TODO(), marte)
	if err != nil {
		t.Fatal(err)
	}

	repository := NewPlanetRepository(client.Database(TESTING_DATABASE))

	t.Run("success", func(t *testing.T) {
		planetID := marte.ID.Hex()
		err := repository.Delete(context.TODO(), planetID)
		assert.NoError(t, err)
	})
	t.Run("fail-to-convert-string-to-object-id", func(t *testing.T) {
		err := repository.Delete(context.TODO(), "109283")
		assert.Error(t, err)
	})
	t.Run("fail-planet-not-found", func(t *testing.T) {
		planetID := primitive.NewObjectID().Hex()
		err := repository.Delete(context.TODO(), planetID)
		assert.Error(t, err)
		assert.Equal(t, "The requested Planet is not found", err.Error())
	})

	// teardown
	_, err = collection.DeleteOne(context.TODO(), bson.D{{"_id", marte.ID}})
	if err != nil {
		t.Fatal(err)
	}
}
