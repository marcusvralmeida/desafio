package router

import (
	"log"
	"net/http"

	"bitbucket.org/marcusvralmeida/desafio-b2w/config"
	"bitbucket.org/marcusvralmeida/desafio-b2w/endpoints"
	_ "bitbucket.org/marcusvralmeida/desafio-b2w/statik"
	"github.com/gorilla/mux"
	"github.com/rakyll/statik/fs"
)

// NewRouter cria um mux.Router
// Recebe configs uma instancia de config.Configurations, e uma instancia de endpoints.Endpoints
func NewRouter(configs config.Configurations, endpoints *endpoints.Endpoints) *mux.Router {
	router := mux.NewRouter()
	// HealthCheck
	router.Path("/health").HandlerFunc(endpoints.HealhCheckEndpoint).Methods("GET")

	// SwaggerUI gerado pelo Statik
	statikFS, err := fs.New()
	if err != nil {
		log.Fatal(err)
	}
	staticServer := http.FileServer(statikFS)
	sh := http.StripPrefix("/swaggerui/", staticServer)
	router.PathPrefix("/swaggerui/").Handler(sh)

	s := router.StrictSlash(true).PathPrefix(configs.Server.PathPrefix).Subrouter()
	s.Path("/").HandlerFunc(endpoints.ListAllPlanetsEndpoint).Methods("GET")
	s.Path("/").Queries("page", "{[0-9]*?}").HandlerFunc(endpoints.ListAllPlanetsEndpoint).Methods("GET")
	s.Path("/").Queries("page", "{[0-9]*?}", "per_page", "{[0-9]*?}").HandlerFunc(endpoints.ListAllPlanetsEndpoint).Methods("GET")
	s.Path("/{id:[0-9a-fA-F]{24}}/").HandlerFunc(endpoints.GetPlanetByIDEndpoint).Methods("GET")
	s.Path("/{name:[a-zA-Z]+}/").HandlerFunc(endpoints.GetPlanetByNameEndpoint).Methods("GET")
	s.Path("/").HandlerFunc(endpoints.SavePlanetEndpoint).Methods("POST")
	s.Path("/{id:[0-9a-fA-F]{24}}/").HandlerFunc(endpoints.DeletePlanetEndpoint).Methods("DELETE")

	return router
}
