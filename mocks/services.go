package mocks

import (
	"context"

	"bitbucket.org/marcusvralmeida/desafio-b2w/entities"
	"github.com/stretchr/testify/mock"
)

// SWAPIClient Mock para client da SWAPI.
type SWAPIClient struct {
	mock.Mock
}

// CountPlanetAppearances método mockado do SWAPIClient.
func (s *SWAPIClient) CountPlanetAppearances(ctx context.Context, planet *entities.Planet) (int, error) {
	args := s.Called(ctx, planet)

	var numberOfAppearances int
	if rf, ok := args.Get(0).(func(context.Context, *entities.Planet) int); ok {
		numberOfAppearances = rf(ctx, planet)
	} else {
		if args.Get(0) != nil {
			numberOfAppearances = args.Get(0).(int)
		}
	}

	var err error
	if rf, ok := args.Get(0).(func(context.Context, *entities.Planet) error); ok {
		err = rf(ctx, planet)
	} else {
		err = args.Error(1)
	}
	return numberOfAppearances, err

}
