package mocks

import (
	"context"

	"bitbucket.org/marcusvralmeida/desafio-b2w/entities"
	mock "github.com/stretchr/testify/mock"
)

// Repository mock
type Repository struct {
	mock.Mock
}

// FindAll método mockado do Repository
func (m *Repository) FindAll(ctx context.Context, page, pageSize int) (*entities.PlanetPaginator, error) {
	args := m.Called(ctx)

	var planetsFound *entities.PlanetPaginator
	if rf, ok := args.Get(0).(func(context.Context, int, int) *entities.PlanetPaginator); ok {
		planetsFound = rf(ctx, page, pageSize)
	} else {
		if args.Get(0) != nil {
			planetsFound = args.Get(0).(*entities.PlanetPaginator)
		}
	}

	var err error
	if rf, ok := args.Get(1).(func(context.Context, int, int) error); ok {
		err = rf(ctx, page, pageSize)
	} else {
		err = args.Error(1)
	}
	return planetsFound, err
}

// FindByID método mockado do Repository
func (m *Repository) FindByID(ctx context.Context, id string) (*entities.Planet, error) {
	args := m.Called(ctx, id)

	var planetFound *entities.Planet
	if rf, ok := args.Get(0).(func(context.Context, string) *entities.Planet); ok {
		planetFound = rf(ctx, id)
	} else {
		if args.Get(0) != nil {
			planetFound = args.Get(0).(*entities.Planet)
		}
	}

	var err error
	if rf, ok := args.Get(1).(func(context.Context, string) error); ok {
		err = rf(ctx, id)
	} else {
		err = args.Error(1)
	}
	return planetFound, err
}

// FindByName método mockado do Repository
func (m *Repository) FindByName(ctx context.Context, name string) (*entities.Planet, error) {
	args := m.Called(ctx, name)

	var planetFound *entities.Planet
	if rf, ok := args.Get(0).(func(context.Context, string) *entities.Planet); ok {
		planetFound = rf(ctx, name)
	} else {
		if args.Get(0) != nil {
			planetFound = args.Get(0).(*entities.Planet)
		}
	}

	var err error
	if rf, ok := args.Get(1).(func(context.Context, string) error); ok {
		err = rf(ctx, name)
	} else {
		err = args.Error(1)
	}
	return planetFound, err
}

// Save método mockado do Repository
func (m *Repository) Save(ctx context.Context, planet *entities.Planet) (*entities.Planet, error) {
	args := m.Called(ctx, planet)

	var planetCreated *entities.Planet
	if rf, ok := args.Get(0).(func(context.Context, *entities.Planet) *entities.Planet); ok {
		planetCreated = rf(ctx, planet)
	} else {
		if args.Get(0) != nil {
			planetCreated = args.Get(0).(*entities.Planet)
		}
	}

	var err error
	if rf, ok := args.Get(1).(func(context.Context, *entities.Planet) error); ok {
		err = rf(ctx, planet)
	} else {
		err = args.Error(1)
	}
	return planetCreated, err
}

// Delete método mockado do Repository
func (m *Repository) Delete(ctx context.Context, id string) error {
	args := m.Called(ctx, id)

	var err error
	if rf, ok := args.Get(0).(func(context.Context, string) error); ok {
		err = rf(ctx, id)
	} else {
		err = args.Error(0)
	}

	return err
}
