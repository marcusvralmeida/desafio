# desafio-b2w

## Proposta:

Para possibilitar a equipe de front criar essa aplicação, queremos desenvolver uma API que contenha os dados dos planetas. 

Requisitos:

- A API deve ser REST

- Para cada planeta, os seguintes dados devem ser obtidos do banco de dados da aplicação, sendo inserido manualmente:

    - Nome
    - Clima
    - Terreno

- Para cada planeta também devemos ter a quantidade de aparições em filmes, que podem ser obtidas pela API pública do Star Wars: https://swapi.co/



Funcionalidades desejadas: 

- Adicionar um planeta (com nome, clima e terreno)

- Listar planetas

- Buscar por nome

- Buscar por ID

- Remover planeta

## Implementação:

Para implementar as funcionalidades propostas utilizamos um modelo de arquitetura seguindo alguns principios do "[Clean Architecture](http://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)". O objetivo principal desse modelo é a separação de conceitos. Essa separação é alcançada dividindo o software em camadas.

 Uma das idéias desse modelo é criar sistemas desacoplados em que a implementação de domínio do négocio (regras de negócio - políticas) fique no centro, independente dos outros detalhes como: frameworks, banco de dados e infraestrutura utilizados no restante do projeto. 
 
 Toda a "comunicação" entre as camadas do software é realizada através de interfaces o que torna o sistema desacoplado facilitando a testabilidade.

 Como toda a "comunicação" entre as camadas é realizada através de interfaces, utilizamos o conceito de inversão de dependências para realizar a "ligação" entre os componentes da aplicação.

 No caso desse projeto, no centro da arquitetura está a entidade ["entities.Planet"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/entities/planet.go). 
 
 Para disponibilizar as funcionalidades desejadas definimos uma interface ["usecases.Usecases"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/usecases/planet.go#lines-15) onde estão as funções:
	
1. ListAll(ctx context.Context, page, pageSize int) (*entities.PlanetPaginator, error): 
    
    Buscar todos os Planetas

2. GetByID(ctx context.Context, id string) (*entities.Planet, error): 
    
    Buscar Planeta por ID

3. GetByName(ctx context.Context, name string) (*entities.Planet, error): 

    Buscar Planeta por Nome

4. Save(ctx context.Context, planet *entities.Planet) (*entities.Planet, error):

    Salvar Planeta

5. Delete(ctx context.Context, id string) error:

    Apagar Planeta

*Obs.: Todos os metodos recebem uma instancia de context.Context para tornar "cancelável"

A implementação dessa interface é ["usecase.PlanetUsecases"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/usecases/planet.go#lines-27), no seu construtor recebe uma instância de um repositório de Planetas, que implementa  ["repositories.PlanetRepository"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/repositories/planet.go#lines-23) e uma instância do cliente para API SW, que implementa ["services.SWAPIClient"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/b43db2e42b0c61d5a817d41db00a3f01d157d16c/services/swapi.go#lines-37).

No repositório de Planetas ["repositories.PlanetRepository"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/repositories/planet.go#lines-23) estão definidas as funções de acesso aos dados.

1. FindAll(ctx context.Context, page, pageSize int) (*entities.PlanetPaginator, error)

    Buscar todos os Planetas. Essa função implementa uma paginação dos dados.

2. FindByID(ctx context.Context, id string) (*entities.Planet, error)

    Buscar um Planeta por ID

3. FindByName(ctx context.Context, name string) (*entities.Planet, error)

    Buscar um Planeta por Nome. Essa função realiza a busca em modo "case insensitive"

4. Save(ctx context.Context, planet *entities.Planet) (*entities.Planet, error)

    Salva um Planeta. Verificando antes se existe algum Planeta cadastrado com mesmo nome (utiliza FindByName)

5. Delete(ctx context.Context, id string) error

    Apagar um Planeta.

*Obs.: Todos os metodos recebem uma instancia de context.Context para tornar "cancelável"

Para o repositório de dados criamos uma implementação ["repositories.MongoDBRespository"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/repositories/planet.go#lines-32) que utiliza o MongoDB como banco de dados.

Para o cliente da API SW foram implementados dois tipos de clientes, seguindo a interface ["services.SWAPIClient"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/b43db2e42b0c61d5a817d41db00a3f01d157d16c/services/swapi.go#lines-37), um cliente sem cache ["services.SWAPIClientImpl"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/b43db2e42b0c61d5a817d41db00a3f01d157d16c/services/swapi.go#lines-42) e outro com cache ["services.CachedSWAPIClientImpl"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/b43db2e42b0c61d5a817d41db00a3f01d157d16c/services/swapi.go#lines-88). O cliente com cache é um "decorator" para o cliente sem cache.


Para disponibilizar o acesso aos dados via HTTP foram implementados os endpoints(HTTP handlers),  em ["endpoints.Endpoints"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/endpoints/planet.go). No construtor de ["endpoints.Endpoints"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/endpoints/planet.go) ele recebe uma instância que implementa a interface ["usecase.PlanetUsecases"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/usecases/planet.go#lines-27). Os endpoints são:

1. ListAllPlanetsEndpoint(w http.ResponseWriter, r *http.Request)

    http handler para listar os Planetas

2. GetPlanetByIDEndpoint(w http.ResponseWriter, r *http.Request)

    http handler para buscar um Planeta por ID

3. GetPlanetByNameEndpoint(w http.ResponseWriter, r *http.Request)
    
    Http handler para buscar uma Planeta por Nome

4. SavePlanetEndpoint(w http.ResponseWriter, r *http.Request)

    http handler para salvar um Planeta

5. DeletePlanetEndpoint(w http.ResponseWriter, r *http.Request)

    http handler para apagar um Planeta

Esses endpoints estão mapeados para as rotas definidas em ["router.NewRouter"](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/router/router.go) que criar uma *mux.Router.

 #### Tecnologias utilizadas:
 
    - Go 1.11.x
    - godep
    - Docker
    - docker-compose
    - MongoDB
    - Memcached

## Estrutura de diretórios:
1. config:

    Contém os "packages" com as definições de "structs" utilizadas pelo [Viper](https://github.com/spf13/viper) para carregamento da configurações contida do arquivo "config.yaml"

2. endpoints:

    Contém o "package" com a implementação dos endpoints (handlers) para o projeto.

3. entities:

    Contém o "package" onde estão definidos as entidades (models) para o projeto.

4. errors:

    Contém o "package" onde estão definidos erros customizados utilizados no projeto.

5. infra:

    Contém os "packages" onde estão implementadas as funções para criar os clientes para o banco de dados (MongoDB) e para o cache(Memcached).

6. middleware:
    
    Contém o "package" onde está implementada a cadeia de middleware utilizada no projeto.

7. mocks:
    Contém os "packages" com os "mocks" utilizados nos testes unitários.

8. repositories:
    
    Contém o "package" onde está implementado o "repository".

9. router:

    Contém o "package" onde estão declaradas as rotas para os endpoints do projeto.

10. services:

    Contém o "package" com a implementação do cliente para a API [https://swapi.co/](https://swapi.co/)

11. statik:

    Contém o "package" com os estáticos do "SwaggerUI" encodados utilizando o projeto [Statik](https://github.com/rakyll/statik)

12. swaggerui:

    Contém os estáticos do "SwaggerUI" bem como a definição da api utilizando o padrão OpenAPI 3.0 [openapi.json](https://bitbucket.org/marcusvralmeida/desafio-b2w/src/master/swaggerui/openapi.json)

13. usecases:

    Contém o "package" com a implementação dos "usecases" utilizados no projeto.

14. Demais arquivos:
    - docker-componse.yml:
        
        Declaração da "stack" de componentes para execução do projeto.

    - Dockerfile:
        Arquivo com "script" de "build" para a imagem do container do projeto.
    
    - Gopkg.toml e Gopkg.lock:
        Arquivos utilizados pelo [dep](https://github.com/golang/dep)
    
    - main.go:
        "Package" main com implementação do executável do projeto.
    
    - Makefile:
        Arquivo com as tasks para facilitar a execução do projeto.


## Como executar:

Para executar o projeto foi criado um Makefile com algumas tasks como o objetivo de facilitar a execução.

As tasks são:
1. Para executar os testes:
```
$ make test
```
2. Para executar o projeto:
```
$ make run
```
3. Para finalizar a execução do projeto:
```
$ make stop
```
4. Para visualizar os logs do projeto:
```
$ make logs
```
5. Criar "package" com estáticos (html, css, js) do SwaggerUI encodados:
```
$ make statik
``` 

Obs.: Para realizar a execução do projeto será necessário ter instalado no ambiente o docker e docker-compose.

## Exemplo de uso
Com a projeto executando é possível acessar os seguintes endpoints:

1. [http://localhost:7070/health/](http://localhost:7070/health/)

    Nesse endpoint encontra-se o "healtcheck" do projeto. Ao realizar um GET, se o projeto estiver executando, será retornado uma resposta com o seguinte formato:
    ```javascript
    {
        "alive": "ok"
    }
    ```

2. [http://localhost:7070/swaggerui/](http://localhost:7070/swaggerui/)
    
    Nesse endpoint encontra-se a interface do SwaggerUI com as definições da API implementada no projeto.

3. Para listar os Planetas cadastrados:
    ```
    $ curl -H "Content-Type:application/json" "http://localhost:7070/v1/planets/"
    ```

    * Para realizar a paginação devemos utilizar os parametros page e per_page
    ```
    $ curl -H "Content-Type:application/json" "http://localhost:7070/v1/planets/?page=1&per_page=2
    ```
    Obs.: Os parâmetros de paginação são opcionais. E caso o parâmetro page for informado será considerado per_page = 10.
    
4. Para salvar um Planeta:
    ```
    curl -XPOST -d '{"name": "Alderaan", "climate": "temperate", "terrain":"grasslands, mountains"}' -H "Content-Type: application/json" "http://localhost:7070/v1/planets/"
    ```

5. Para buscar um Planeta por ID:
    ```
    curl -H "Content-Type:application/json" "http://localhost:7070/v1/planets/5c2e7156aadd09eac50fc902/"
    ```

6. Para buscar um Planeta por nome:
    ```
    curl -H "Content-Type:application/json" "http://localhost:7070/v1/planets/Hoth/"
    ```
    Obs.: A busca por nome é "case insensitive"

7. Para apagar um Planeta:
    ```
    curl -XDELETE "http://localhost:7070/v1/planets/5c2e7156aadd09eac50fc902/"
    ``` 

### Observações:

1. Não foi implementada uma modelo da autenticação, pois essa funcionalidade poderia ser responsabilidade de um API GATEWAY.

2. Sobre a implmentação da paginação, utilizamos os métodos Skip e Limit da API do MongoDD. Apesar de não ser muito performática, por realizar a busca de todos os registros e depois realizar o "Skip" dos dados. Como o o número de planetas disponíveis não é muito grande não teria um grande impacto na performance da aplicação.
