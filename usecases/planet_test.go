package usecases

import (
	"context"
	"testing"
	"time"

	"github.com/mongodb/mongo-go-driver/bson/primitive"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"bitbucket.org/marcusvralmeida/desafio-b2w/entities"
	errors "bitbucket.org/marcusvralmeida/desafio-b2w/errors"
	"bitbucket.org/marcusvralmeida/desafio-b2w/mocks"
)

func TestFindAll(t *testing.T) {
	mockPlanetRepo := new(mocks.Repository)
	mockServiceClient := new(mocks.SWAPIClient)

	mockListPlanets := make([]*entities.Planet, 0)
	mockListPlanets = append(mockListPlanets, &entities.Planet{
		Name:            "Alderaan",
		Climate:         "temperate",
		Terrain:         "grasslands, mountains",
		FilmAppearances: 2,
	})
	mockListPlanets = append(mockListPlanets, &entities.Planet{
		Name:            "Hoth",
		Climate:         "frozen",
		Terrain:         "tundra, ice caves, mountain ranges",
		FilmAppearances: 1,
	})
	paginator := &entities.PlanetPaginator{
		Total:    2,
		Next:     0,
		Previous: 0,
		Results:  mockListPlanets,
	}

	t.Run("success", func(t *testing.T) {
		mockPlanetRepo.On("FindAll", mock.Anything, mock.Anything, mock.Anything).Return(paginator, nil).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		planets, err := u.ListAll(context.TODO(), 1, 2)
		assert.NoError(t, err)
		assert.NotEmpty(t, planets.Results)
		assert.Len(t, planets.Results, len(mockListPlanets))

		mockPlanetRepo.AssertExpectations(t)
	})
	t.Run("error-failed", func(t *testing.T) {
		mockPlanetRepo.On("FindAll", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.ErrInternalServerError).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		planets, err := u.ListAll(context.TODO(), 1, 2)
		assert.Error(t, err)
		assert.Nil(t, planets)

		mockPlanetRepo.AssertExpectations(t)
	})
}
func TestFindByName(t *testing.T) {
	mockPlanetRepo := new(mocks.Repository)
	mockPlanet := entities.Planet{
		Name:            "Hoth",
		Climate:         "frozen",
		Terrain:         "tundra, ice caves, mountain ranges",
		FilmAppearances: 1,
	}
	mockServiceClient := new(mocks.SWAPIClient)

	t.Run("success", func(t *testing.T) {
		mockPlanetRepo.On("FindByName", mock.Anything, mock.AnythingOfType("string")).Return(&mockPlanet, nil).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		planet, err := u.GetByName(context.TODO(), mockPlanet.Name)
		assert.NoError(t, err)
		assert.NotNil(t, planet)

		mockPlanetRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {
		mockPlanetRepo.On("FindByName", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrInternalServerError).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		planet, err := u.GetByName(context.TODO(), mockPlanet.Name)
		assert.Error(t, err)
		assert.Nil(t, planet)

		mockPlanetRepo.AssertExpectations(t)
	})
}

func TestFindByID(t *testing.T) {
	mockPlanetRepo := new(mocks.Repository)
	objectID := primitive.NewObjectID()
	mockPlanet := entities.Planet{
		ID:              objectID,
		Name:            "Hoth",
		Climate:         "frozen",
		Terrain:         "tundra, ice caves, mountain ranges",
		FilmAppearances: 1,
	}
	mockServiceClient := new(mocks.SWAPIClient)

	t.Run("success", func(t *testing.T) {
		mockPlanetRepo.On("FindByID", mock.Anything, mock.AnythingOfType("string")).Return(&mockPlanet, nil).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		planet, err := u.GetByID(context.TODO(), objectID.Hex())
		assert.NoError(t, err)
		assert.NotNil(t, planet)

		mockPlanetRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {
		mockPlanetRepo.On("FindByID", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrInternalServerError).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		planet, err := u.GetByID(context.TODO(), mockPlanet.ID.Hex())
		assert.Error(t, err)
		assert.Nil(t, planet)

		mockPlanetRepo.AssertExpectations(t)
	})
}

func TestSave(t *testing.T) {
	mockPlanetRepo := new(mocks.Repository)
	mockServiceClient := new(mocks.SWAPIClient)
	mockPlanet := entities.Planet{
		ID:              primitive.NewObjectID(),
		Name:            "Hoth",
		Climate:         "frozen",
		Terrain:         "tundra, ice caves, mountain ranges",
		FilmAppearances: 1,
	}
	t.Run("success", func(t *testing.T) {
		mockPlanetRepo.On("FindByName", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrNotFound).Once()
		mockServiceClient.On("CountPlanetAppearances", mock.Anything, mock.AnythingOfType("*entities.Planet")).Return(1, nil).Once()
		mockPlanetRepo.On("Save", mock.Anything, mock.AnythingOfType("*entities.Planet")).Return(&mockPlanet, nil).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		planet := &entities.Planet{
			Name:    "Hoth",
			Climate: "frozen",
			Terrain: "tundra, ice caves, mountain ranges",
		}
		planetCreated, err := u.Save(context.TODO(), planet)

		assert.NoError(t, err)
		assert.NotNil(t, planetCreated.ID)
		assert.Equal(t, "Hoth", planetCreated.Name)

		mockServiceClient.AssertExpectations(t)
		mockPlanetRepo.AssertExpectations(t)
	})
	t.Run("error-exist-planet-with-same-name", func(t *testing.T) {
		existingPlanet := mockPlanet
		mockPlanetRepo.On("FindByName", mock.Anything, mock.AnythingOfType("string")).Return(&existingPlanet, nil).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)
		planet := &entities.Planet{
			Name:    "Hoth",
			Climate: "frozen",
			Terrain: "tundra, ice caves, mountain ranges",
		}
		_, err := u.Save(context.TODO(), planet)

		assert.Error(t, err)
		mockPlanetRepo.AssertExpectations(t)
	})
	t.Run("error-planet-with-name-not-found-on-swapi", func(t *testing.T) {
		mockPlanetRepo.On("FindByName", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrNotFound).Once()
		mockServiceClient.On("CountPlanetAppearances", mock.Anything, mock.AnythingOfType("*entities.Planet")).Return(nil, errors.ErrInternalServerError).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)
		planet := &entities.Planet{
			Name:    "Hoth",
			Climate: "frozen",
			Terrain: "tundra, ice caves, mountain ranges",
		}
		_, err := u.Save(context.TODO(), planet)

		assert.Error(t, err)
		mockPlanetRepo.AssertExpectations(t)
		mockServiceClient.AssertExpectations(t)
	})
	t.Run("error-failed", func(t *testing.T) {
		mockPlanetRepo.On("FindByName", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrNotFound).Once()
		mockServiceClient.On("CountPlanetAppearances", mock.Anything, mock.AnythingOfType("*entities.Planet")).Return(2, nil).Once()
		mockPlanetRepo.On("Save", mock.Anything, mock.AnythingOfType("*entities.Planet")).Return(nil, errors.ErrInternalServerError).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		planet := &entities.Planet{
			Name:    "Hoth",
			Climate: "frozen",
			Terrain: "tundra, ice caves, mountain ranges",
		}
		_, err := u.Save(context.TODO(), planet)

		assert.Error(t, err)
		mockServiceClient.AssertExpectations(t)
		mockPlanetRepo.AssertExpectations(t)
	})
}

func TestDelete(t *testing.T) {
	mockPlanetRepo := new(mocks.Repository)
	mockServiceClient := new(mocks.SWAPIClient)

	mockPlanet := entities.Planet{
		ID:              primitive.NewObjectID(),
		Name:            "Hoth",
		Climate:         "frozen",
		Terrain:         "tundra, ice caves, mountain ranges",
		FilmAppearances: 1,
	}

	t.Run("success", func(t *testing.T) {
		mockPlanetRepo.On("FindByID", mock.Anything, mock.AnythingOfType("string")).Return(&mockPlanet, nil).Once()

		mockPlanetRepo.On("Delete", mock.Anything, mock.AnythingOfType("string")).Return(nil).Once()

		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		err := u.Delete(context.TODO(), mockPlanet.ID.Hex())

		assert.NoError(t, err)
		mockPlanetRepo.AssertExpectations(t)
	})
	t.Run("planet-not-found", func(t *testing.T) {
		mockPlanetRepo.On("FindByID", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrNotFound).Once()
		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		err := u.Delete(context.TODO(), mockPlanet.ID.Hex())
		assert.Error(t, err)
		mockPlanetRepo.AssertExpectations(t)
	})
	t.Run("error-in-database-calling-find-by-id", func(t *testing.T) {
		mockPlanetRepo.On("FindByID", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrInternalServerError).Once()
		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		err := u.Delete(context.TODO(), mockPlanet.ID.Hex())
		assert.Error(t, err)
		mockPlanetRepo.AssertExpectations(t)
	})
	t.Run("error-in-database-calling-delete", func(t *testing.T) {
		mockPlanetRepo.On("FindByID", mock.Anything, mock.AnythingOfType("string")).Return(&mockPlanet, nil).Once()
		mockPlanetRepo.On("Delete", mock.Anything, mock.AnythingOfType("string")).Return(errors.ErrInternalServerError).Once()
		u := NewPlanetUsecases(mockPlanetRepo, mockServiceClient, 5*time.Second)

		err := u.Delete(context.TODO(), mockPlanet.ID.Hex())
		assert.Error(t, err)
		mockPlanetRepo.AssertExpectations(t)
	})
}
