package usecases

import (
	"context"
	"log"
	"time"

	"bitbucket.org/marcusvralmeida/desafio-b2w/entities"
	errors "bitbucket.org/marcusvralmeida/desafio-b2w/errors"
	"bitbucket.org/marcusvralmeida/desafio-b2w/repositories"
	"bitbucket.org/marcusvralmeida/desafio-b2w/services"
)

// Usecase interface que define as funções utilizadas nos casos de uso da aplicação.
type Usecase interface {
	ListAll(ctx context.Context, page, pageSize int) (*entities.PlanetPaginator, error)
	GetByID(ctx context.Context, id string) (*entities.Planet, error)
	GetByName(ctx context.Context, name string) (*entities.Planet, error)
	Save(ctx context.Context, planet *entities.Planet) (*entities.Planet, error)
	Delete(ctx context.Context, id string) error
}

// PlanetUsecase struct que implementa a interface Usecase
// Contém um repository que é uma interface de repositories.PlanetRepository
// Contém um service que é uma interface de  services.SWAPIClient
// Contém um contextTimeout que é um time.Duration para cancelamento do context.
type PlanetUsecase struct {
	repository     repositories.PlanetRepository
	service        services.SWAPIClient
	contextTimeout time.Duration
}

// NewPlanetUsecases construtor.
func NewPlanetUsecases(repo repositories.PlanetRepository, srv services.SWAPIClient, timeout time.Duration) Usecase {
	return &PlanetUsecase{
		repository:     repo,
		service:        srv,
		contextTimeout: timeout,
	}
}

// ListAll função que implmenta o usecase listar todos os Planetas.
// Recebe um context.Context e os parâmentros para paginação page e pageSize.
// Retorna o ponteiro para entities.PlanetPaginator ou um erro.
func (p *PlanetUsecase) ListAll(ctx context.Context, page, pageSize int) (*entities.PlanetPaginator, error) {
	ctx, cancel := context.WithTimeout(ctx, p.contextTimeout)
	defer cancel()

	res, err := p.repository.FindAll(ctx, page, pageSize)
	if err != nil {
		log.Println("PlanetUsecase - ListAll: ", err.Error())
		return nil, err
	}
	return res, nil
}

// GetByID função que implementa o usecase Buscar Planeta por id.
// Recebe um context.Context e o id do Planeta que quer consultar.
// Retorna o ponteiro para entities.Planet ou um erro.
func (p *PlanetUsecase) GetByID(ctx context.Context, id string) (*entities.Planet, error) {
	ctx, cancel := context.WithTimeout(ctx, p.contextTimeout)
	defer cancel()

	res, err := p.repository.FindByID(ctx, id)
	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			return nil, errors.ErrNotFound
		}
		log.Println("PlanetUsecase - GetByID: ", err.Error())
		return nil, err
	}

	return res, nil
}

// GetByName função que implementa o usecase Buscar Planeta por Nome.
// Recebe um context.Context e o nome do Planeta que quer consultar.
// Retorna o ponteiro para entities.Planet ou um erro.
func (p *PlanetUsecase) GetByName(ctx context.Context, name string) (*entities.Planet, error) {
	ctx, cancel := context.WithTimeout(ctx, p.contextTimeout)
	defer cancel()

	res, err := p.repository.FindByName(ctx, name)
	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			return nil, errors.ErrNotFound
		}
		log.Println("PlanetUsecase - GetByName: ", err.Error())
		return nil, err
	}

	return res, nil
}

// Save função que implementa o usecase Salvar um Planeta.
// Recebe um context.Context e uma instância de entities.Planet.
// Retorna o ponteiro para entities.Planet salvo ou um erro.
func (p *PlanetUsecase) Save(ctx context.Context, planet *entities.Planet) (*entities.Planet, error) {
	ctx, cancel := context.WithTimeout(ctx, p.contextTimeout)
	defer cancel()

	existedPlanet, _ := p.repository.FindByName(ctx, planet.Name)
	if existedPlanet != nil {
		return nil, errors.ErrConflict
	}

	numberOfAppearances, err := p.service.CountPlanetAppearances(ctx, planet)
	if err != nil {
		log.Println("PlanetUsecase - Save: ", err.Error())
		return nil, err
	}

	planet.FilmAppearances = numberOfAppearances

	if planetCreated, err := p.repository.Save(ctx, planet); err != nil {
		log.Println("PlanetUsecase - Save: ", err.Error())
		return nil, err
	} else {
		return planetCreated, nil
	}
}

// Delete função que implementa o usecase Apagar um Planeta.
// Recebe um context.Context e o id do Planeta que se quer apagar.
// Retorna um erro no caso de operação não ser concluída, caso contrário nil.
func (p *PlanetUsecase) Delete(ctx context.Context, id string) error {
	ctx, cancel := context.WithTimeout(ctx, p.contextTimeout)
	defer cancel()

	_, err := p.repository.FindByID(ctx, id)
	if err != nil {
		log.Println("PlanetUsecase - Delete: ", err.Error())
		return err
	}

	return p.repository.Delete(ctx, id)
}
