package config

// ServerConfigurations struct utilizada para as configurações do servidor http da aplicação.
type ServerConfigurations struct {
	Port       int
	PathPrefix string `mapstructure:"path_prefix"`
	JWTSecret  string `mapstructure:"token_secret"`
	Cors       CORS
}

// CORS settings struct utilizada para as configurações de CORS
type CORS struct {
	AllowedOrigins []string `mapstructure:"allow_origins"`
	AllowedHeaders []string `mapstructure:"allow_headers"`
	AllowedMethods []string `mapstructure:"allow_methods"`
}
