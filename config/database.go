package config

// DatabaseConfigurations struct utilizada para as configurações do MongoDB
type DatabaseConfigurations struct {
	Host       string
	Port       string
	Name       string
	Collection string
}
