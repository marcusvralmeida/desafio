package config

// MemcachedConfigurations struct utilizada para as configurações do Memcached
type MemcachedConfigurations struct {
	Host       string
	Port       string
	Expiration int
}
