package config

// Configurations struct utilizada para as configurações da aplicação.
type Configurations struct {
	Server   ServerConfigurations
	Database DatabaseConfigurations
	Cache    MemcachedConfigurations
}
