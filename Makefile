BINARY_NAME=planets
GOCMD=go
GOTEST=$(GOCMD) test

dep:
	@dep ensure -v

test: dep
	$(GOTEST) -v -cover -covermode=atomic ./...

statik: dep
	@statik -src=/home/marcus/go/src/bitbucket.org/marcusvralmeida/desafio-b2w/swaggerui -f
	
docker:
	docker build -t planets-api .

run: docker
	docker-compose up -d

stop:
	docker-compose down

remove_containers:
	docker-compose rm -a

logs:
	docker-compose logs -f 
