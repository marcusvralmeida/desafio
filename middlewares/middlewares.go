package middlewares

import (
	"net/http"
	"os"

	"github.com/gorilla/mux"

	"bitbucket.org/marcusvralmeida/desafio-b2w/config"
	"github.com/gorilla/handlers"
)

// Middlewares retorna um http.Handler após a configuração da cadeia de middlewares utilizados na aplicação
func Middlewares(configs config.Configurations, h *mux.Router) http.Handler {
	// CORS handler
	handler := handlers.CORS(handlers.AllowedHeaders(configs.Server.Cors.AllowedHeaders),
		handlers.AllowedMethods(configs.Server.Cors.AllowedMethods),
		handlers.AllowedOrigins(configs.Server.Cors.AllowedOrigins))(h)

	// Logger handler
	handler = handlers.LoggingHandler(os.Stdout, handler)

	// Recovery handler
	handler = handlers.RecoveryHandler()(handler)

	return handler
}
