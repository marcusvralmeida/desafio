package services

import (
	"context"
	"crypto/tls"
	"net"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/marcusvralmeida/desafio-b2w/entities"
	"github.com/stretchr/testify/assert"
)

const (
	okResponse = `{
		"count": 1,
		"next": null,
		"previous": null,
		"results": [
			{
				"name": "Hoth",
				"rotation_period": "23",
				"orbital_period": "549",
				"diameter": "7200",
				"climate": "frozen",
				"gravity": "1.1 standard",
				"terrain": "tundra, ice caves, mountain ranges",
				"surface_water": "100",
				"population": "unknown",
				"residents": [],
				"films": [
					"https://swapi.co/api/films/2/"
				],
				"created": "2014-12-10T11:39:13.934000Z",
				"edited": "2014-12-20T20:58:18.423000Z",
				"url": "https://swapi.co/api/planets/4/"
			}
		]
	}`

	moreThanOnePlanetResponse = `{
		"count": 1,
		"next": null,
		"previous": null,
		"results": [
			{
				"name": "Hoth",
				"rotation_period": "23",
				"orbital_period": "549",
				"diameter": "7200",
				"climate": "frozen",
				"gravity": "1.1 standard",
				"terrain": "tundra, ice caves, mountain ranges",
				"surface_water": "100",
				"population": "unknown",
				"residents": [],
				"films": [
					"https://swapi.co/api/films/2/"
				],
				"created": "2014-12-10T11:39:13.934000Z",
				"edited": "2014-12-20T20:58:18.423000Z",
				"url": "https://swapi.co/api/planets/4/"
			},
			{
				"name": "Bespin", 
				"rotation_period": "12", 
				"orbital_period": "5110", 
				"diameter": "118000", 
				"climate": "temperate", 
				"gravity": "1.5 (surface), 1 standard (Cloud City)", 
				"terrain": "gas giant", 
				"surface_water": "0", 
				"population": "6000000", 
				"residents": [
					"https://swapi.co/api/people/26/"
				], 
				"films": [
					"https://swapi.co/api/films/2/"
				], 
				"created": "2014-12-10T11:43:55.240000Z", 
				"edited": "2014-12-20T20:58:18.427000Z", 
				"url": "https://swapi.co/api/planets/6/"
			}
		]
	}`
	notFoundResponse = `{
		"count": 0,
		"next": null,
		"previous": null,
		"results": []
	}`
)

func fakeHTTPClient(handler http.Handler) (*http.Client, func()) {
	s := httptest.NewTLSServer(handler)

	cli := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
			DialContext: func(_ context.Context, network, _ string) (net.Conn, error) {
				return net.Dial(network, s.Listener.Addr().String())
			},
		},
	}

	return cli, s.Close
}

func TestCountPlanetAppearances(t *testing.T) {
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(okResponse))
	})
	httpClient, teardown := fakeHTTPClient(h)
	defer teardown()

	cli := NewSWAPIClient()
	cli.httpClient = httpClient
	planet := entities.NewPlanet("Hoth", "frozen", "tundra, ice caves, mountain ranges", 0)
	movieAppearances, err := cli.CountPlanetAppearances(context.TODO(), planet)
	assert.Nil(t, err)
	assert.Equal(t, 1, movieAppearances)
}

func TestCountPlanetAppearancesWhenPlanetNotExist(t *testing.T) {
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(notFoundResponse))
	})
	httpClient, teardown := fakeHTTPClient(h)
	defer teardown()

	cli := NewSWAPIClient()
	cli.httpClient = httpClient
	planet := entities.NewPlanet("Urano", "frozen", "tundra, ice caves, mountain ranges", 0)
	movieAppearances, err := cli.CountPlanetAppearances(context.TODO(), planet)
	assert.NotNil(t, err)
	assert.Equal(t, 0, movieAppearances)
}

func TestCountPlanetAppearancesWhenReturnMoreThanOnePlanet(t *testing.T) {
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(moreThanOnePlanetResponse))
	})
	httpClient, teardown := fakeHTTPClient(h)
	defer teardown()

	cli := NewSWAPIClient()
	cli.httpClient = httpClient
	planet := entities.NewPlanet("U", "frozen", "tundra, ice caves, mountain ranges", 0)
	movieAppearances, err := cli.CountPlanetAppearances(context.TODO(), planet)
	assert.NotNil(t, err)
	assert.Equal(t, "SWAPI: More than one Planet found", err.Error())
	assert.Equal(t, 0, movieAppearances)
}
