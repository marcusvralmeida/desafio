package services

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/marcusvralmeida/desafio-b2w/entities"
	"github.com/bradfitz/gomemcache/memcache"
)

const (
	BASEURL = "https://swapi.co/api/planets"
)

type Result struct {
	Name  string   `json:"name"`
	Films []string `json:"films"`
}

type ResponseAPI struct {
	Count    int      `json:"count"`
	Next     string   `json:"next"`
	Previous string   `json:"previous"`
	Results  []Result `json:"results"`
}

// SWAPIClient ...
type SWAPIClient interface {
	CountPlanetAppearances(ctx context.Context, planet *entities.Planet) (int, error)
}

// SWAPIClientImpl ...
type SWAPIClientImpl struct {
	httpClient *http.Client
}

// NewSWAPIClient ...
func NewSWAPIClient() *SWAPIClientImpl {
	return &SWAPIClientImpl{
		httpClient: &http.Client{
			Timeout: 5 * time.Second,
		},
	}
}

// CountPlanetAppearances ...
func (cli SWAPIClientImpl) CountPlanetAppearances(ctx context.Context, planet *entities.Planet) (int, error) {

	req, err := http.NewRequest("GET", fmt.Sprintf("%s?search=%s", BASEURL, strings.Title(planet.Name)), nil)
	req = req.WithContext(ctx)

	if err != nil {
		log.Println("SWAPIClientImpl: ", err.Error())
		return 0, errors.New("SWAPI: failed to build request")
	}

	response, err := cli.httpClient.Do(req)
	if err != nil {
		log.Println("SWAPIClientImpl: ", err.Error())
		return 0, errors.New("SWAPI: request failed")
	}

	res := new(ResponseAPI)

	if err := json.NewDecoder(response.Body).Decode(&res); err != nil {
		if err == io.EOF {
			log.Println("SWAPIClientImpl:  SWAPI: Planet not found")
			return 0, errors.New("SWAPI: Planet not found")
		}
		log.Println("SWAPIClientImpl: ", err.Error())
		return 0, errors.New("SWAPI: unmarshaling failed")
	}

	if len(res.Results) == 0 {
		log.Println("SWAPIClientImpl:  SWAPI: Planet not found")
		return 0, errors.New("SWAPI: Planet not found")
	}

	if len(res.Results) > 1 {
		log.Println("SWAPIClientImpl: SWAPI: More than one Planet found")
		return 0, errors.New("SWAPI: More than one Planet found")
	}

	return len(res.Results[0].Films), nil
}

// CachedSWAPIClientImpl ...
type CachedSWAPIClientImpl struct {
	APIClient SWAPIClient
	Cache     *memcache.Client
	Timeout   int
}

// NewCachedSWAPIClient ...
func NewCachedSWAPIClient(client SWAPIClient, cacheClient *memcache.Client, timeoutInSeconds int) *CachedSWAPIClientImpl {
	return &CachedSWAPIClientImpl{
		client,
		cacheClient,
		timeoutInSeconds,
	}
}

// CountPlanetAppearances cached api client.
func (cli CachedSWAPIClientImpl) CountPlanetAppearances(ctx context.Context, planet *entities.Planet) (int, error) {
	key := cli.CacheKey(planet.Name)
	cached, err := cli.Cache.Get(key)
	if err != nil {
		switch err {
		case memcache.ErrCacheMiss:
			if numberAppearences, err := cli.APIClient.CountPlanetAppearances(ctx, planet); err != nil {
				log.Println("CachedSWAPIClientImpl: ", err.Error())
				return 0, err
			} else {
				item := &memcache.Item{Key: key, Value: []byte(strconv.Itoa(numberAppearences)), Expiration: int32(cli.Timeout)}
				err := cli.Cache.Set(item)
				if err != nil {
					log.Println("CachedSWAPIClientImpl: ", err.Error())
					return 0, err
				}
				return numberAppearences, nil
			}

		default:
			log.Println("CachedSWAPIClientImpl: ", err.Error())
			return 0, err
		}
	}
	cachedValue, _ := strconv.Atoi(string(cached.Value))
	return cachedValue, nil
}

// CacheKey make cache key.
func (cli CachedSWAPIClientImpl) CacheKey(s string) string {
	hasher := md5.New()
	hasher.Write([]byte(s))
	return hex.EncodeToString(hasher.Sum(nil))
}
