###############################
# STEP 1 build do executavel #
###############################
FROM golang:alpine AS builder 

RUN  apk add --no-cache ca-certificates git && \
     update-ca-certificates && \
     go get -u github.com/golang/dep/cmd/dep

# CREATE appuser.
RUN adduser -D -g '' appuser

WORKDIR /go/src/bitbucket.org/marcusvralmeida/desafio-b2w

COPY . ./

RUN dep ensure 

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o main -v 

#################################
# STEP 2 build de imagem menor #
#################################

FROM scratch

COPY --from=builder /etc/passwd /etc/passwd 
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ 

# COPY our static executable
COPY --from=builder ./go/src/bitbucket.org/marcusvralmeida/desafio-b2w/main ./  
COPY --from=builder ./go/src/bitbucket.org/marcusvralmeida/desafio-b2w/config.yaml ./

USER appuser

ENV PORT 7070
EXPOSE 7070

ENTRYPOINT [ "/main" ]
