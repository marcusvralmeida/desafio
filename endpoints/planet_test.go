package endpoints

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/mongodb/mongo-go-driver/bson/primitive"

	"github.com/stretchr/testify/mock"

	"bitbucket.org/marcusvralmeida/desafio-b2w/entities"
	errors "bitbucket.org/marcusvralmeida/desafio-b2w/errors"
	"bitbucket.org/marcusvralmeida/desafio-b2w/mocks"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestHealthCheckEndpoint(t *testing.T) {
	mockUCase := new(mocks.Usecase)
	endpoints := NewEndpoints(mockUCase)

	t.Run("success", func(t *testing.T) {
		request, _ := http.NewRequest("GET", "/health", nil)
		response := httptest.NewRecorder()

		router := mux.NewRouter()
		router.HandleFunc("/health", endpoints.HealhCheckEndpoint).Methods("GET")
		router.ServeHTTP(response, request)

		statusCodeExpected := 200
		assert.Equal(t, statusCodeExpected, response.Code, "200 response is expected")

		bodyExpected := `{"alive":"OK"}`
		assert.Equal(t, bodyExpected, response.Body.String(), "JSON with ok is expected")
	})
	t.Run("fail-method-not-allowed", func(t *testing.T) {
		request, _ := http.NewRequest("POST", "/health", nil)
		response := httptest.NewRecorder()

		router := mux.NewRouter()
		router.HandleFunc("/health", endpoints.HealhCheckEndpoint).Methods("GET")
		router.ServeHTTP(response, request)

		statusCodeExpected := 405
		assert.Equal(t, statusCodeExpected, response.Code, "405 response is expected")
	})
}

func TestListAllPlanetsEndpoint(t *testing.T) {
	mockUCase := new(mocks.Usecase)
	mockListPlanets := make([]*entities.Planet, 0)
	mockListPlanets = append(mockListPlanets, &entities.Planet{
		Name:            "Alderaan",
		Climate:         "temperate",
		Terrain:         "grasslands, mountains",
		FilmAppearances: 2,
	})
	mockListPlanets = append(mockListPlanets, &entities.Planet{
		Name:            "Hoth",
		Climate:         "frozen",
		Terrain:         "tundra, ice caves, mountain ranges",
		FilmAppearances: 1,
	})
	endpoints := NewEndpoints(mockUCase)

	t.Run("success", func(t *testing.T) {
		paginator := &entities.PlanetPaginator{
			Total:    len(mockListPlanets),
			Next:     0,
			Previous: 0,
			Results:  mockListPlanets,
		}
		mockUCase.On("ListAll", mock.Anything, mock.Anything, mock.Anything).Return(paginator, nil).Once()

		request, _ := http.NewRequest("GET", "/", nil)
		response := httptest.NewRecorder()

		router := mux.NewRouter()
		router.HandleFunc("/", endpoints.ListAllPlanetsEndpoint).Methods("GET")
		router.ServeHTTP(response, request)

		statusCodeExpected := 200
		assert.Equal(t, statusCodeExpected, response.Code, "200 response is expected")

		expected := struct {
			Count    int                `json:"count"`
			Previous string             `json:"previous,omitempt"`
			Next     string             `json:"next,omitempt"`
			Results  []*entities.Planet `json:"results"`
		}{
			Count:    paginator.Total,
			Previous: paginator.PreviousPage(request.Host, request.URL.Path),
			Next:     paginator.NextPage(request.Host, request.URL.Path),
			Results:  paginator.Results,
		}
		b, err := json.Marshal(expected)
		assert.NoError(t, err)
		bodyExpected := string(b)

		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})

	t.Run("success-paginating", func(t *testing.T) {
		paginator := &entities.PlanetPaginator{
			Total:    len(mockListPlanets[:1]),
			Next:     1,
			Previous: 0,
			Results:  mockListPlanets[:1],
		}
		mockUCase.On("ListAll", mock.Anything, mock.Anything, mock.Anything).Return(paginator, nil).Once()

		request, _ := http.NewRequest("GET", "/?page=1&page_size=1", nil)
		response := httptest.NewRecorder()

		router := mux.NewRouter()
		router.Path("/").HandlerFunc(endpoints.ListAllPlanetsEndpoint).Methods("GET")
		router.ServeHTTP(response, request)

		statusCodeExpected := 200
		assert.Equal(t, statusCodeExpected, response.Code, "200 response is expected")

		expected := struct {
			Count    int                `json:"count"`
			Previous string             `json:"previous,omitempt"`
			Next     string             `json:"next,omitempt"`
			Results  []*entities.Planet `json:"results"`
		}{
			Count:    paginator.Total,
			Previous: paginator.PreviousPage(request.Host, request.URL.Path),
			Next:     paginator.NextPage(request.Host, request.URL.Path),
			Results:  paginator.Results,
		}
		b, err := json.Marshal(expected)
		assert.NoError(t, err)
		bodyExpected := string(b)

		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})

	t.Run("error-on-usecase", func(t *testing.T) {
		mockUCase.On("ListAll", mock.Anything).Return(nil, errors.ErrInternalServerError).Once()

		request, _ := http.NewRequest("GET", "/", nil)
		response := httptest.NewRecorder()

		endpoints := NewEndpoints(mockUCase)

		router := mux.NewRouter()
		router.HandleFunc("/", endpoints.ListAllPlanetsEndpoint).Methods("GET")
		router.ServeHTTP(response, request)

		statusCodeExpected := 500
		assert.Equal(t, statusCodeExpected, response.Code, "500 response is expected")

		bodyExpected := `{"error":"Internal Server Error"}`
		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})
}

func TestGetPlanetByIDEndpoint(t *testing.T) {
	mockPlanet := entities.Planet{
		ID:              primitive.NewObjectID(),
		Name:            "Hoth",
		Climate:         "frozen",
		Terrain:         "tundra, ice caves, mountain ranges",
		FilmAppearances: 1,
	}
	mockUCase := new(mocks.Usecase)
	router := mux.NewRouter()
	endpoints := NewEndpoints(mockUCase)

	router.HandleFunc("/{id:[0-9a-fA-F]{24}}/", endpoints.GetPlanetByIDEndpoint).Methods("GET")

	t.Run("success", func(t *testing.T) {
		mockUCase.On("GetByID", mock.Anything, mock.AnythingOfType("string")).Return(&mockPlanet, nil).Once()

		request, _ := http.NewRequest("GET", "/5c2ce7c94108eb96cfc4393e/", nil)
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 200
		assert.Equal(t, statusCodeExpected, response.Code, "200 response is expected")

		b, err := json.Marshal(mockPlanet)
		assert.NoError(t, err)
		bodyExpected := string(b)

		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})

	t.Run("fail-not-found", func(t *testing.T) {
		mockUCase.On("GetByID", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrNotFound).Once()

		request, _ := http.NewRequest("GET", "/4c2ce7c94108eb96cfc4393e/", nil)
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 404
		assert.Equal(t, statusCodeExpected, response.Code, "404 response is expected")

		bodyExpected := `{"error":"The requested Planet is not found"}`
		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})

	t.Run("fail-unexpected-error", func(t *testing.T) {
		mockUCase.On("GetByID", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrInternalServerError).Once()

		request, _ := http.NewRequest("GET", "/4c2ce7c94108eb96cfc4393e/", nil)
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 500
		assert.Equal(t, statusCodeExpected, response.Code, "500 response is expected")

		bodyExpected := `{"error":"Internal Server Error"}`
		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})
}

func TestGetPlanetByNameEndpoint(t *testing.T) {
	mockPlanet := entities.Planet{
		ID:              primitive.NewObjectID(),
		Name:            "Hoth",
		Climate:         "frozen",
		Terrain:         "tundra, ice caves, mountain ranges",
		FilmAppearances: 1,
	}
	mockUCase := new(mocks.Usecase)
	router := mux.NewRouter()
	endpoints := NewEndpoints(mockUCase)
	router.HandleFunc("/{name:[a-zA-Z]+}/", endpoints.GetPlanetByNameEndpoint).Methods("GET")

	t.Run("success", func(t *testing.T) {
		mockUCase.On("GetByName", mock.Anything, mock.AnythingOfType("string")).Return(&mockPlanet, nil).Once()

		request, _ := http.NewRequest("GET", "/Hoth/", nil)
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 200
		assert.Equal(t, statusCodeExpected, response.Code, "200 response is expected")

		b, err := json.Marshal(mockPlanet)
		assert.NoError(t, err)
		bodyExpected := string(b)

		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})

	t.Run("fail-not-found", func(t *testing.T) {
		mockUCase.On("GetByName", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrNotFound).Once()

		request, _ := http.NewRequest("GET", "/Terra/", nil)
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 404
		assert.Equal(t, statusCodeExpected, response.Code, "404 response is expected")

		bodyExpected := `{"error":"The requested Planet is not found"}`
		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})

	t.Run("fail-unexpected-error", func(t *testing.T) {
		mockUCase.On("GetByName", mock.Anything, mock.AnythingOfType("string")).Return(nil, errors.ErrInternalServerError).Once()

		request, _ := http.NewRequest("GET", "/Hoth/", nil)
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 500
		assert.Equal(t, statusCodeExpected, response.Code, "500 response is expected")

		bodyExpected := `{"error":"Internal Server Error"}`
		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})
}

func TestSavePlanetEndpoint(t *testing.T) {
	mockUCase := new(mocks.Usecase)
	mockPlanet := entities.Planet{
		ID:              primitive.NewObjectID(),
		Name:            "Hoth",
		Climate:         "frozen",
		Terrain:         "tundra, ice caves, mountain ranges",
		FilmAppearances: 1,
	}
	router := mux.NewRouter()
	endpoints := NewEndpoints(mockUCase)
	router.HandleFunc("/planets", endpoints.SavePlanetEndpoint).Methods("POST")

	t.Run("success", func(t *testing.T) {
		mockUCase.On("Save", mock.Anything, mock.AnythingOfType("*entities.Planet")).Return(&mockPlanet, nil).Once()

		jsonPlanet := []byte(`{
			"name":    "Hoth",
			"climate": "frozen",
			"terrain": "tundra, ice caves, mountain ranges"
		}`)

		request, _ := http.NewRequest("POST", "/planets", bytes.NewBuffer(jsonPlanet))
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 201
		assert.Equal(t, statusCodeExpected, response.Code, "201 response is expected")

		var planetCreated entities.Planet
		err := json.NewDecoder(response.Body).Decode(&planetCreated)
		assert.NoError(t, err)
		assert.NotNil(t, planetCreated.ID)
		assert.Equal(t, "Hoth", planetCreated.Name)

		mockUCase.AssertExpectations(t)
	})

	t.Run("fail-invalid-data-required-fields", func(t *testing.T) {
		jsonPlanet := []byte(`{
			"climate": "frozen",
			"terrain": "tundra, ice caves, mountain ranges"
		}`)

		request, _ := http.NewRequest("POST", "/planets", bytes.NewBuffer(jsonPlanet))
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 400
		assert.Equal(t, statusCodeExpected, response.Code, "400 response is expected")

		bodyExpected := `{"error":"'name' is required."}`
		assert.Equal(t, bodyExpected, response.Body.String(), "JSON with ok is expected")
	})

	t.Run("fail-invalid-format-data", func(t *testing.T) {
		jsonPlanet := []byte(`{
			"clima": "frozen",
			"terreno": "tundra, ice caves, mountain ranges",
			"Nome":    "Hoth",
			"Numero"
		}`)
		request, _ := http.NewRequest("POST", "/planets", bytes.NewBuffer(jsonPlanet))
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 400
		assert.Equal(t, statusCodeExpected, response.Code, "400 response is expected")

		bodyExpected := `{"error":"Post Params are not valid"}`
		assert.Equal(t, bodyExpected, response.Body.String(), "JSON with ok is expected")
	})

	t.Run("fail-planet-already-exists", func(t *testing.T) {
		mockUCase.On("Save", mock.Anything, mock.AnythingOfType("*entities.Planet")).Return(nil, errors.ErrConflict).Once()

		jsonPlanet := []byte(`{
			"name":    "Hoth",
			"climate": "frozen",
			"terrain": "tundra, ice caves, mountain ranges"
		}`)

		request, _ := http.NewRequest("POST", "/planets", bytes.NewBuffer(jsonPlanet))
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 409
		assert.Equal(t, statusCodeExpected, response.Code, "409 response is expected")

		bodyExpected := `{"error":"A Planet with this name already exist"}`
		assert.Equal(t, bodyExpected, response.Body.String(), "JSON with ok is expected")

		mockUCase.AssertExpectations(t)
	})
}

func TestDeletePlanetEndpoint(t *testing.T) {
	mockUCase := new(mocks.Usecase)
	router := mux.NewRouter()
	endpoints := NewEndpoints(mockUCase)
	router.HandleFunc("/{id:[0-9a-fA-F]{24}}/", endpoints.DeletePlanetEndpoint).Methods("DELETE")

	t.Run("success", func(t *testing.T) {
		mockUCase.On("Delete", mock.Anything, mock.AnythingOfType("string")).Return(nil).Once()

		request, _ := http.NewRequest("DELETE", "/5c2ce7c94108eb96cfc4393e/", nil)
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 204
		assert.Equal(t, statusCodeExpected, response.Code, "204 response is expected")
		mockUCase.AssertExpectations(t)
	})

	t.Run("fail-not-found", func(t *testing.T) {
		mockUCase.On("Delete", mock.Anything, mock.AnythingOfType("string")).Return(errors.ErrNotFound).Once()

		request, _ := http.NewRequest("DELETE", "/5c2ce7c94108eb96cfc4393e/", nil)
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 404
		assert.Equal(t, statusCodeExpected, response.Code, "404 response is expected")

		bodyExpected := `{"error":"The requested Planet is not found"}`
		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})

	t.Run("fail-unexpected-error", func(t *testing.T) {
		mockUCase.On("Delete", mock.Anything, mock.AnythingOfType("string")).Return(errors.ErrInternalServerError).Once()

		request, _ := http.NewRequest("DELETE", "/5c2ce7c94108eb96cfc4393e/", nil)
		response := httptest.NewRecorder()

		router.ServeHTTP(response, request)

		statusCodeExpected := 500
		assert.Equal(t, statusCodeExpected, response.Code, "500 response is expected")

		bodyExpected := `{"error":"Internal Server Error"}`
		assert.Equal(t, bodyExpected, response.Body.String(), "Result is expected")
		mockUCase.AssertExpectations(t)
	})

}
