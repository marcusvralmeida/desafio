package endpoints

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	"bitbucket.org/marcusvralmeida/desafio-b2w/entities"
	errors "bitbucket.org/marcusvralmeida/desafio-b2w/errors"
	"bitbucket.org/marcusvralmeida/desafio-b2w/usecases"
)

// PerPage items por página ná paginação.
const PerPage = 10

// Endpoints struct que implementa os endpoints para HTTP.
type Endpoints struct {
	PlanetUsecase usecases.Usecase
}

// NewEndpoints construtor
// Recebe uma instância de uma implementação de usecases.Usecase
// Retorna um ponteiro para a struct Endpoints
func NewEndpoints(us usecases.Usecase) *Endpoints {
	return &Endpoints{
		PlanetUsecase: us,
	}
}

// HealhCheckEndpoint http handler para o "health check" da aplicação.
func (h *Endpoints) HealhCheckEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"alive":"OK"}`))
}

// ListAllPlanetsEndpoint http handler para listar Planetas
func (h *Endpoints) ListAllPlanetsEndpoint(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	host := r.Host
	path := r.URL.Path

	var page, perPage int
	p := r.FormValue("page")
	if p != "" {
		page, _ = strconv.Atoi(p)
	} else {
		page = 1
	}
	pp := r.FormValue("per_page")
	if p != "" {
		perPage, _ = strconv.Atoi(pp)
	} else {
		perPage = PerPage
	}

	paginator, err := h.PlanetUsecase.ListAll(ctx, page, perPage)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	paginator.PerPage = perPage
	listOfPlanets := struct {
		Count    int                `json:"count"`
		Previous string             `json:"previous,omitempt"`
		Next     string             `json:"next,omitempt"`
		Results  []*entities.Planet `json:"results"`
	}{
		Count:    paginator.Total,
		Previous: paginator.PreviousPage(host, path),
		Next:     paginator.NextPage(host, path),
		Results:  paginator.Results,
	}
	respondWithJSON(w, http.StatusOK, listOfPlanets)
}

// GetPlanetByIDEndpoint http handler para buscar Planeta por Id.
func (h *Endpoints) GetPlanetByIDEndpoint(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	requestParams := mux.Vars(r)
	planetFound, err := h.PlanetUsecase.GetByID(ctx, requestParams["id"])
	if err != nil {
		switch err {
		case errors.ErrNotFound:
			respondWithError(w, http.StatusNotFound, err.Error())
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	respondWithJSON(w, http.StatusOK, planetFound)
}

// GetPlanetByNameEndpoint http handler para buscar Planeta por Nome.
func (h *Endpoints) GetPlanetByNameEndpoint(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	requestParams := mux.Vars(r)
	planetFound, err := h.PlanetUsecase.GetByName(ctx, requestParams["name"])
	if err != nil {
		switch err {
		case errors.ErrNotFound:
			respondWithError(w, http.StatusNotFound, err.Error())
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	respondWithJSON(w, http.StatusOK, planetFound)
}

// SavePlanetEndpoint http handler para salvar um Planeta.
func (h *Endpoints) SavePlanetEndpoint(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	defer r.Body.Close()

	var planet entities.Planet
	if err := json.NewDecoder(r.Body).Decode(&planet); err != nil {
		respondWithError(w, http.StatusBadRequest, errors.ErrBadParamsInput.Error())
		return
	}

	if err := planet.IsValid(); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	if planetCreated, err := h.PlanetUsecase.Save(ctx, &planet); err != nil {
		switch err {
		case errors.ErrConflict:
			respondWithError(w, http.StatusConflict, err.Error())
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	} else {
		respondWithJSON(w, http.StatusCreated, planetCreated)
	}
}

// DeletePlanetEndpoint http handler para apagar um Planeta.
func (h *Endpoints) DeletePlanetEndpoint(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	requestParams := mux.Vars(r)
	err := h.PlanetUsecase.Delete(ctx, requestParams["id"])

	if err != nil {
		switch err {
		case errors.ErrNotFound:
			respondWithError(w, http.StatusNotFound, err.Error())
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}
	respondWithJSON(w, http.StatusNoContent, nil)

}

// ResponseError struct que representa um response de erro.
type ResponseError struct {
	Message string `json:"message"`
}

// respondWithError função para retornar uma mensagem de erro.
func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJSON(w, code, map[string]string{"error": msg})
}

// respondWithJSON função para retornar um JSON.
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := JSONMarshal(payload, true)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

// JSONMarshal função para encoding seguro.
func JSONMarshal(v interface{}, safeEncoding bool) ([]byte, error) {
	b, err := json.Marshal(v)

	if safeEncoding {
		b = bytes.Replace(b, []byte("\\u0026"), []byte("&"), -1)
	}
	return b, err
}
