package main

import (
	"fmt"
	"log"

	"net/http"
	"time"

	"bitbucket.org/marcusvralmeida/desafio-b2w/middlewares"

	"bitbucket.org/marcusvralmeida/desafio-b2w/config"
	"bitbucket.org/marcusvralmeida/desafio-b2w/endpoints"
	"bitbucket.org/marcusvralmeida/desafio-b2w/infra"
	"bitbucket.org/marcusvralmeida/desafio-b2w/repositories"
	"bitbucket.org/marcusvralmeida/desafio-b2w/router"
	"bitbucket.org/marcusvralmeida/desafio-b2w/services"
	"bitbucket.org/marcusvralmeida/desafio-b2w/usecases"
	"github.com/TV4/graceful"
	"github.com/spf13/viper"
)

var configs config.Configurations

// Carrega as configurações da aplicação a partir do arquivo "config.yaml" setando a variável configs.
func init() {
	viper.SetConfigFile("config.yaml")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		panic(fmt.Errorf("fatal error config file: %s ", err))
	}

	if err := viper.Unmarshal(&configs); err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
}

func main() {
	repository := repositories.NewPlanetRepository(infra.DatabaseConnect(configs))
	swAPIClient := services.NewCachedSWAPIClient(services.NewSWAPIClient(), infra.CacheClient(configs), configs.Cache.Expiration)

	usecases := usecases.NewPlanetUsecases(repository, swAPIClient, 5*time.Second)

	endpoints := endpoints.NewEndpoints(usecases)

	router := middlewares.Middlewares(configs, router.NewRouter(configs, endpoints))

	server := &http.Server{
		Handler:      router,
		Addr:         fmt.Sprintf(":%d", configs.Server.Port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		IdleTimeout:  60 * time.Second,
	}

	graceful.LogListenAndServe(server)
}
