package infra

import (
	"fmt"

	"bitbucket.org/marcusvralmeida/desafio-b2w/config"
	"github.com/bradfitz/gomemcache/memcache"
)

// CacheClient retorna o client do cache a partir das configurações contidas da struct configs
func CacheClient(configs config.Configurations) *memcache.Client {
	mc := memcache.New(fmt.Sprintf("%s:%s", configs.Cache.Host, configs.Cache.Port))
	return mc
}
