package infra

import (
	"context"
	"fmt"
	"log"
	"time"

	"bitbucket.org/marcusvralmeida/desafio-b2w/config"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/readpref"
)

// DatabaseConnect retorna a conexão com o banco de dados, neste caso MongoDB.
func DatabaseConnect(configs config.Configurations) *mongo.Database {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	connString := fmt.Sprintf("mongodb://%s:%s", configs.Database.Host, configs.Database.Port)
	client, err := mongo.Connect(ctx, connString)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal("Could not connect on MongoDB")
	}
	return client.Database(configs.Database.Name)
}
