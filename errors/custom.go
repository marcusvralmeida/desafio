package errors

import "errors"

// Erros customizados.
var (
	// ErrInternalServerError
	ErrInternalServerError = errors.New("Internal Server Error")
	// ErrNotFound
	ErrNotFound = errors.New("The requested Planet is not found")
	// ErrConflict
	ErrConflict = errors.New("A Planet with this name already exist")
	// ErrBadParamsInput
	ErrBadParamsInput = errors.New("Post Params are not valid")
)
